﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebPlataforma.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login Plataforma</title>
	

    <link href="Styles/Login.css" rel="stylesheet" />

    <script src="Scripts/jquery-3.5.1.min.js"></script>
    <script src="Scripts/Login.js"></script>
    
</head>
<body>
	
	<div id="id_bloquear" class="div_bloquear">Procesando ... espere por favor</div>            
    <div class="contenedor flex_column">
        <form class="frm_login" id="frm_login" method="post" autocomplete="off">
            <h1 class="title_form">
                Bienvenido a la Plataforma de Gestion
            </h1>
            <br />
            <br />
            <div>
                <label class="label_form" for="txt_usuario">Usuario</label>
                <br />
                <input type="text" class="input_form" id="txt_usuario" placeholder="USUARIO" runat="server"/>
            </div>
            <br />
            <div>
                <label class="label_form" for="txt_password">Password</label>
                <br />
                <input type="password" class="input_form" id="txt_password" placeholder="CONTRASEÑA" runat="server"/>
            </div>
            <br />
            <br />
            <button type="submit" class="btn_form" id="btn_ingresar">Ingresar</button>
            <br />
        </form>
    </div>

</body>
</html>
