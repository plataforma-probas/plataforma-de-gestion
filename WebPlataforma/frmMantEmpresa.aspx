﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Master.Master" AutoEventWireup="true" CodeBehind="frmMantEmpresa.aspx.cs" Inherits="WebPlataforma.frmMantEmpresa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/frmMantEmpresa.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="box-header">
        <h3 class="box-title" id="tituloID" style="font-size: 30px;font-weight: 600;color: #1b1818; text-transform: none;">Mantenedor de Empresas</h3>
    </div>
    <br />

    <div class="div_tabla">
        <table id="tbl_empresa" class="table table-striped table-bordered" style="width: 100%;">
            <thead>
                <tr>
                    <th>CODIGO</th>
                    <th>RAZON SOCIAL</th>
                    <th>RUC</th>
                    <th>DIRECCION</th>
                    <th>REPRESENTANTE</th>                    
                    <th>ACCION</th>
                </tr>
            </thead>
                
            <tbody id="tbl_body_table">
                <tr>
                    <td>1</td>
                    <td>IDE-SOLUTION S.A.C.</td>
                    <td>20561160369</td>
                    <td>CHICLAYO - LORETO 168</td>
                    <td>RUBEN PACHAMANGO</td>
                    <td>Editar | Eliminar</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>PROBAS</td>
                    <td>20511160369</td>
                    <td>LIMA - LORETO 168</td>
                    <td>JUAN PEREZ</td>
                    <td>Editar | Eliminar</td>
                </tr>
            </tbody>
        </table>
        <br />
        <button type="button" class="btn_tabla" id="buttonModal" data-toggle="modal" data-target="#reg_empresa"  >
                AGREGAR
        </button>
    </div>
    <br />

    <div class="modal fade" id="reg_empresa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style=" width: 750px !important">
            <div class="modal-content" style="padding:15px;">
                <form class="div_bloque" id="frm_registrar_promo">
                    <div class="title_bloque">Registrar Empresa</div>
                    <br />
                    <label class="label_bloque">Razon Social</label>
                    <input type="text" class="input_bloque" />
                    <br />
                    <label class="label_bloque">Ruc</label>
                    <input type="text" class="input_bloque" />
                    <br />
                    <label class="label_bloque">Direccion</label>
                    <input type="text" class="input_bloque" />
                    <br />
                    <label class="label_bloque">Representante</label>
                    <input type="text" class="input_bloque" />
                    <br />
                    <label class="label_bloque">Accion</label>
                    <input type="text" class="input_bloque" />
                    <br />
                    <button type="button" class="btn_bloque" data-dismiss="modal" id="btnCancelaq">Cancelar</button>
                    <button type="submit" class="btn_bloque" id="btnGuardar">Guardar</button>
                    <br />
                </form>
            </div>
        </div>
    </div>

</asp:Content>
