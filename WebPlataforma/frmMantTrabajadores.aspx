﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web_Master.Master" AutoEventWireup="true" CodeBehind="frmMantTrabajadores.aspx.cs" Inherits="WebPlataforma.frmMantTrabajadores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/frmMantTrabajadores.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="box-header">
        <h3 class="box-title" id="tituloID" style="font-size: 30px;font-weight: 600;color: #1b1818; text-transform: none;">Mantenedor de Trabajadores</h3>
    </div>
    <br />

    <div class="div_bloque">
        <div class="title_bloque">Filtros de Búsqueda</div>
        <br />
        <label for="idSelect2" class="label_bloque">Empresa</label>
        <select class="cbo_bloque">
            <option value="0">-SELECCIONAR-</option>                                   
        </select>
        <br />
        <label class="label_bloque">DNI</label>
        <input type="text" class="input_bloque" />
        <br />
        <label class="label_bloque">NOMBRES</label>
        <input type="text" class="input_bloque" />
        <br />
        <button type="button" class="btn_bloque" id="id_btnBuscar" >BUSCAR</button>
    </div>
    <br />
    
    <div class="div_tabla">
        <table id="tbl_trabajador" class="table table-striped table-bordered" style="width: 100%;">
            <thead>
                <tr>
                    <th>EMPRESA</th>
                    <th>DNI</th>
                    <th>NOMBRES</th>
                    <th>APELLIDO PATERNO</th>
                    <th>APELLIDO MATERNO</th>
                    <th>DIRECCION</th>
                    <th>USUARIO</th>
                    <th>CONTRASEÑA</th>
                    <th>FECHA INGRESO</th>
                    <th>SALARIO</th>
                    <th>ACCION</th>
                </tr>
            </thead>
                
            <tbody id="tbl_body_table">
                <tr>
                    <td>IDE-SOLUTION S.A.C.</td>
                    <td>12345678</td>
                    <td>FLEMING</td>
                    <td>ALFARO</td>
                    <td>PARY</td>
                    <td>CHICLAYO - LORETO 168</td>
                    <td>FALFARO</td>
                    <td>ABCD1234</td>
                    <td>01/07/2020</td>
                    <td>1 500.00</td>
                    <td>Editar | Eliminar</td>
                </tr>
                <tr>
                    <td>IDE-SOLUTION S.A.C.</td>
                    <td>87654321</td>
                    <td>RUBEN</td>
                    <td>PACHAMANGO</td>
                    <td>CHUNQUE</td>
                    <td>LAMBAYEQUE - LORETO 168</td>
                    <td>RPACHAMANGO</td>
                    <td>ABCD1234</td>
                    <td>15/07/2020</td>
                    <td>1 500.00</td>
                    <td>Editar | Eliminar</td>
                </tr>
            </tbody>
        </table>
        <br />
        <br />

        <form class="form-inline">
	        <div class="form-group">
		        <label for="files">Buscar Archivo CSV :</label>
		        <input type="file" id="files" class="form-control" accept=".csv" required />
	        </div>
	        <div class="form-group">
		        <button type="submit" id="submit-file" class="btn btn-primary">Procesar</button>
	        </div>
        </form>
    </div>
    <br />
</asp:Content>
