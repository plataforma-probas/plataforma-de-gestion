﻿$(document).ready(function () {

    var tbl_trabajador = $("#tbl_trabajador");

    tbl_trabajador.DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        }
    });

    var tb_trabajador = tbl_trabajador.DataTable();
    tb_trabajador.columns.adjust().draw();
});