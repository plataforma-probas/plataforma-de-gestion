﻿$(document).ready(function () {

    var tbl_empresa = $("#tbl_empresa");

    tbl_empresa.DataTable({
        "paging": false,
        "info": false,
        "language": {
            search: "Busqueda Rapida:",
            infoEmpty: "No hay registros disponibles.",
            zeroRecords: "No hay registros"
        }
    });

    var tb_empresa = tbl_empresa.DataTable();
    tb_empresa.columns.adjust().draw();
});